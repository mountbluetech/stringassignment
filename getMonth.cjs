function getMonth(date) {
    return date.substring(date.indexOf('/')+1, date.lastIndexOf('/'));
}


module.exports = getMonth;