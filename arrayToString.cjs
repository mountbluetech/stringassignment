function arrayToString(Strings) {
    let result = "";
    
    for(let element of Strings) {
        result += element+" ";
    }    
    return result;
}

module.exports = arrayToString;