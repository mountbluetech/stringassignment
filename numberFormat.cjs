function numberFormat(num){
    let formatted = '';
    for(let char in num){
        if(!isNaN(num[char]) || num[char] =='.' || num[char] =='-') formatted += num[char];
        else if(num[char] == '$' || num[char] == ',') continue;
        else return 0;
    }
    return formatted;
}

module.exports = numberFormat;