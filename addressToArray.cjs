
function addressToArray(address){
    let result = [];
    let ele = '';
    for(let char in address){
        if(address[char] == '.'){
            result.push(ele);
            ele = '';
        }
        else ele += address[char];
    }
    result.push(ele);
    return result;
}

module.exports = addressToArray;