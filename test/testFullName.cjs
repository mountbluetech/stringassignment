let first = {"first_name": "JoHN", "last_name": "SMith"}
let second = {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}
let fullName = require('../fullName.cjs')

console.log(fullName(first));
console.log(fullName(second));