

function fullName(nameObject) {
    let fullname = '';
    if(nameObject.first_name) fullname += nameObject.first_name+" ";
    if(nameObject.middle_name) fullname += nameObject.middle_name + " ";
    if(nameObject.last_name) fullname += nameObject.last_name;
    return fullname.toUpperCase();
}

module.exports = fullName;